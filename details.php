<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_PyroIdeal extends Module {

	public $version = '2.1';

	 public function __construct()
    {
    	// $this->load->library('pyroideal/ideal');
    	$this->load->library('pyroideal/curl');

    }

	public function info()
	{
		return array(
			'name' => array(
				'en' => 'PyroIdeal'
			),
			'description' => array(
				'en' => 'This is a PyroCMS module pyroideal.'
			),
			'frontend' => TRUE,
			'backend' => TRUE,
			'menu' => 'content', // You can also place modules in their top level menu. For example try: 'menu' => 'pyroideal',
			'sections' => array(
				'items' => array(
					'name' 	=> 'pyroideal:items', // These are translated from your language file
					'uri' 	=> 'admin/pyroideal',
						'shortcuts' => array(
							'create' => array(
								'name' 	=> 'pyroideal:create',
								'uri' 	=> 'admin/pyroideal/create',
								'class' => 'add'
								)
							)
						)
				)
		);
	}

	public function install()
	{
		$this->dbforge->drop_table('pyroideal');
		$this->db->delete('settings', array('module' => 'pyroideal'));

		$pyroideal = array(
            'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_endpoint' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_merchant_id' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_sub_id' => array(
				'type' => 'INT',
				'constraint' => '1'
			),
			'ideal_merchant_public_cert' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_merchant_private_cert' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_merchant_private_cert_pass' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_root_public_certs' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_issuer_public_cert' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			),
			'ideal_return_url' => array(
				'type' => 'VARCHAR',
				'constraint' => '100'
			)
		);

		$pyroideal_setting = array(
			'slug' => 'pyroideal_setting',
			'title' => 'Pyroideal Setting',
			'description' => 'A Yes or No option for the pyroideal module',
			'`default`' => '1',
			'`value`' => '1',
			'type' => 'select',
			'`options`' => '1=Yes|0=No',
			'is_required' => 1,
			'is_gui' => 1,
			'module' => 'pyroideal'
		);

		$this->dbforge->add_field($pyroideal);
		$this->dbforge->add_key('id', TRUE);

		if($this->dbforge->create_table('pyroideal'))
		{
			return TRUE;
		}
	}

	public function uninstall()
	{
		$this->dbforge->drop_table('pyroideal');
		$this->db->delete('settings', array('module' => 'pyroideal'));
		{
			return TRUE;
		}
	}


	public function upgrade($old_version)
	{
		// Your Upgrade Logic
		return TRUE;
	}

	public function help()
	{
		// Return a string containing help info
		// You could include a file and return it here.
		return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
	}
}
/* End of file details.php */
