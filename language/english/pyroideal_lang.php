<?php
//messages
$lang['pyroideal:success']			=	'It worked';
$lang['pyroideal:error']			=	'It didn\'t work';
$lang['pyroideal:no_items']			=	'No Items';

//page titles
$lang['pyroideal:create']			=	'Create Item';

//labels
$lang['pyroideal:name']				=	'Name';
$lang['pyroideal:slug']				=	'Slug';
$lang['pyroideal:manage']			=	'Manage';
$lang['pyroideal:item_list']		=	'Item List';
$lang['pyroideal:view']				=	'View';
$lang['pyroideal:edit']				=	'Edit';
$lang['pyroideal:delete']			=	'Delete';

//buttons
$lang['pyroideal:custom_button']	=	'Custom Button';
$lang['pyroideal:items']			=	'Items';

//more labels
$lang['pyroideal:ideal_endpoint']	= 	'ideal_endpoint';
$lang['pyroideal:ideal_merchant_id']	= 	'ideal_merchant_id';
$lang['pyroideal:ideal_sub_id']		= 	'ideal_sub_id';
$lang['pyroideal:ideal_merchant_public_cert']	= 	'ideal_merchant_public_cert';
$lang['pyroideal:ideal_merchant_private_cert']	= 	'ideal_merchant_private_cert';
$lang['pyroideal:ideal_merchant_private_cert_pass']	= 	'ideal_merchant_private_cert_pass';
$lang['pyroideal:ideal_root_public_certs']	= 	'ideal_root_public_certs';
$lang['pyroideal:ideal_issuer_public_cert']	= 	'ideal_issuer_public_cert';
$lang['pyroideal:ideal_return_url']	= 	'ideal_return_url';

?>