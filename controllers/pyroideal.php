<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * This is a pyroideal module for PyroCMS
 *
 * @author 		
 * @website		
 * @package 	
 * @subpackage 	
 */
class PyroIdeal extends Public_Controller
{

	public function __construct()
	{
		parent::__construct();

		$path_to_certificates = FCPATH.SHARED_ADDONPATH.'modules/'.$this->module.'/certificates/';
		// Load the required classes
		$this->load->model('pyroideal_m');
		$this->lang->load('pyroideal');
		$this->load->library('ideal');
		$this->template->append_css('module::pyroideal.css')
		->append_js('module::pyroideal.js');
		$row = $this->pyroideal_m->get('1');
		$this->ideal->set_ideal_merchant_id($row->ideal_merchant_id);
		$this->ideal->set_ideal_sub_id($row->ideal_sub_id);
		$this->ideal->set_ideal_endpoint($row->ideal_endpoint);
		$this->ideal->set_ideal_merchant_private_cert_pass($row->ideal_merchant_private_cert_pass);
		$this->ideal->set_ideal_merchant_private_cert($path_to_certificates.$row->ideal_merchant_private_cert);
		$this->ideal->set_ideal_issuer_public_cert($path_to_certificates.$row->ideal_issuer_public_cert);
		$this->ideal->set_ideal_merchant_public_cert($path_to_certificates.$row->ideal_merchant_public_cert);
		$this->ideal->set_ideal_root_public_certs($path_to_certificates.$row->ideal_root_public_certs);
	}

	/**
	 * All items
	 */
	public function index($offset = 0)
	{
		// set the pagination limit
		$limit = 5;
		
		$data->items = $this->pyroideal_m->limit($limit)
			->offset($offset)
			->get_all();

		$data->aideal = $this->ideal->get_directory();
			
		// we'll do a quick check here so we can tell tags whether there is data or not
		if (count($data->items))
		{
			$data->items_exist = TRUE;
		}
		else
		{
			$data->items_exist = FALSE;
		}

		// we're using the pagination helper to do the pagination for us. Params are: (module/method, total count, limit, uri segment)
		$data->pagination = create_pagination('pyroideal', $this->pyroideal_m->count_all(), $limit, 2);

		$this->template->title($this->module_details['name'], 'the rest of the page title')
						->build('index', $data);
	}
}