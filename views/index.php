<div class="sample-container">

	{{ if items_exist == false }}
		<p>There are no items.</p>
	{{ else }}
		<div class="sample-data">
			<table cellpadding="0" cellspacing="0">
				<tr>
					<th>{{ helper:lang line="sample:name" }}</th>
				</tr>
				<!-- Here we loop through the $items array -->
				{{ items }}
				<tr>
					<td>{{ name }}</td>
				</tr>
				{{ /items }}
			</table>
		</div>

		{{ aideal }}

		{{ pagination:links }}
	
	{{ endif }}
	
</div>