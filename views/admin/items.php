<section class="title">
	<h4><?php echo lang('pyroideal:item_list'); ?></h4>
</section>

<section class="item">
	<?php echo form_open('admin/pyroideal/delete');?>
	
	<?php if (!empty($items)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('pyroideal:name'); ?></th>
					<th><?php echo lang('pyroideal:slug'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $items as $item ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->name; ?></td>
					<td><a href="<?php echo rtrim(site_url(), '/').'/pyroideal'; ?>">
						<?php echo rtrim(site_url(), '/').'/pyroideal'; ?></a></td>
					<td class="actions">
						<?php echo
						anchor('pyroideal', lang('pyroideal:view'), 'class="button" target="_blank"').' '.
						anchor('admin/pyroideal/edit/'.$item->id, lang('pyroideal:edit'), 'class="button"').' '.
						anchor('admin/pyroideal/delete/'.$item->id, 	lang('pyroideal:delete'), array('class'=>'button')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('pyroideal:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
</section>