<section class="title">
	<!-- We'll use $this->method to switch between pyroideal.create & pyroideal.edit -->
	<h4><?php echo lang('pyroideal:'.$this->method); ?></h4>
</section>

<section class="item">

	<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
		
		<div class="form_inputs">
	
		<ul>
			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="name"><?php echo lang('pyroideal:name'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('name', set_value('name', $name), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_endpoint"><?php echo lang('pyroideal:ideal_endpoint'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_endpoint', set_value('ideal_endpoint', $ideal_endpoint), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_merchant_id"><?php echo lang('pyroideal:ideal_merchant_id'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_merchant_id', set_value('ideal_merchant_id', $ideal_merchant_id), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_sub_id"><?php echo lang('pyroideal:ideal_sub_id'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_sub_id', set_value('ideal_sub_id', $ideal_sub_id), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_merchant_public_cert"><?php echo lang('pyroideal:ideal_merchant_public_cert'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_merchant_public_cert', set_value('ideal_merchant_public_cert', $ideal_merchant_public_cert), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_merchant_private_cert"><?php echo lang('pyroideal:ideal_merchant_private_cert'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_merchant_private_cert', set_value('ideal_merchant_private_cert', $ideal_merchant_private_cert), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_merchant_private_cert_pass"><?php echo lang('pyroideal:ideal_merchant_private_cert_pass'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_merchant_private_cert_pass', set_value('ideal_merchant_private_cert_pass', $ideal_merchant_private_cert_pass), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_root_public_certs"><?php echo lang('pyroideal:ideal_root_public_certs'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_root_public_certs', set_value('ideal_root_public_certs', $ideal_root_public_certs), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_issuer_public_cert"><?php echo lang('pyroideal:ideal_issuer_public_cert'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_issuer_public_cert', set_value('ideal_issuer_public_cert', $ideal_issuer_public_cert), 'class="width-15"'); ?></div>
			</li>

			<li class="<?php echo alternator('', 'even'); ?>">
				<label for="ideal_return_url"><?php echo lang('pyroideal:ideal_return_url'); ?> <span>*</span></label>
				<div class="input"><?php echo form_input('ideal_return_url', set_value('ideal_return_url', $ideal_return_url), 'class="width-15"'); ?></div>
			</li>

		</ul>
		
		</div>
		
		<div class="buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )); ?>
		</div>
		
	<?php echo form_close(); ?>

</section>