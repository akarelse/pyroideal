<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @author 		
 * @website		
 * @package 	
 * @subpackage 	
 */
class Pyroideal_m extends MY_Model {

	public function __construct()
	{		
		parent::__construct();

		$this->_table = 'pyroideal';
	}
	
	//create a new item
	public function create($input)
	{
		$to_insert = array(
			'name' => $input['name'],
			'ideal_endpoint' => $input['ideal_endpoint'],
			'ideal_merchant_id' => $input['ideal_merchant_id'],
			'ideal_sub_id' => $input['ideal_sub_id'],
			'ideal_merchant_public_cert' => $input['ideal_merchant_public_cert'],
			'ideal_merchant_private_cert' => $input['ideal_merchant_private_cert'],
			'ideal_merchant_private_cert_pass' => $input['ideal_merchant_private_cert_pass'],
			'ideal_root_public_certs' => $input['ideal_root_public_certs'],
			'ideal_issuer_public_cert' => $input['ideal_issuer_public_cert'],
			'ideal_return_url' => $input['ideal_return_url']
		);

		return $this->db->insert('pyroideal', $to_insert);
	}


}
